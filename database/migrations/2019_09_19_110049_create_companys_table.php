<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companys', function (Blueprint $table) {
            $table->increments('com_id');
            $table->string('com_fullname');
            $table->string('com_shortname');
            $table->string('com_email');
            $table->string('com_email2');
            $table->string('com_tel');
            $table->string('com_tel2');
            $table->string('com_address');
            $table->Text('com_shortdescrpt');
            $table->longText('com_fulldescrpt');
            $table->Text('com_vision');
            $table->Text('com_mission');
            $table->Text('com_value');
            $table->string('com_favicon');
            $table->string('com_logo');
            $table->string('com_facebook');
            $table->string('com_twitter');
            $table->string('com_instagram');
            $table->string('com_linkedin');
            $table->string('com_aboutimage');
            $table->Text('com_keywords');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companys');
    }
}
