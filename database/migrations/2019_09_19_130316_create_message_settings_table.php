<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mail_host');
            $table->string('mail_username');
            $table->string('mail_password');
            $table->string('mail_port');
            $table->string('mail_secure');
            $table->string('mail_debug');
            $table->string('mail_auth');
            $table->string('sms_host');
            $table->string('sms_username');
            $table->string('sms_password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_settings');
    }
}
