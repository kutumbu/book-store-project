<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('pid');
            $table->string('sku')->unique();
            $table->integer('cat_id');
            $table->integer('subcat_id');
            $table->integer('user_id');
            $table->string('url')->unique();
            $table->string('pname');
            $table->string('author');
            $table->string('isbn');
            $table->text('short_descrpt');
            $table->longText('full_descrpt');
            $table->integer('price');
            $table->string('deal');
            $table->string('bestseller');
            $table->string('image');
            $table->integer('status');
            $table->integer('featured');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
