@extends( 'layouts.app' )

@section('title','Create Account')

@section('style')
@endsection

@section('content')
<!-- Start Bradcaump area -->
<div class="ht__bradcaump__area bg-image--6">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bradcaump__inner text-center">
                        	<h2 class="bradcaump-title">Create Account</h2>
                            <nav class="bradcaump-content">
                              <a class="breadcrumb_item" href="{{url('/')}}">Home</a>
                              <span class="brd-separetor">/</span>
                              <span class="breadcrumb_item active">Create Account</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <!-- Start Contact Area -->
        <section class="wn_contact_area bg--white pt--80 pb--80">
			<div class="container">
        		<div class="row">
                <div class="col-lg-2 col-12"></div>
        			<div class="col-lg-8 col-12">
        				<div class="contact-form-wrap">
        					<h2 class="contact__title">Create Your Account</h2>
        					<p>Complete all the fields and click on the button to register account. </p>
                            <form id="register-form">
                            {{ csrf_field() }}
                                <div class="single-contact-form space-between">
                                    <input type="text" name="fname" placeholder="First Name*">
                                    <input type="text" name="lname" placeholder="Last Name*">
                                </div>
                                <div class="single-contact-form space-between">
                                    <input type="email" name="email" placeholder="Email*">
                                    <input type="text" name="tel" placeholder="Phone Number*">
                                </div>
                                <div class="single-contact-form space-between">
                                    <input type="password" name="password" placeholder="Password*">
                                    <input type="password" name="repassword" placeholder="Confirm Password*">
                                </div>
                                 <div class="contact-btn">
                                    <button type="submit">Create Account</button>
                                </div>
                            </form>
                        </div> 
                        <div class="form-output">
                            <p class="form-messege">
                        </div>
        			</div>
        			<div class="col-lg-2 col-12 md-mt-40 sm-mt-40"></div>
        		</div>
        	</div>
        </section>
        <!-- End Contact Area -->
@endsection

@section('script')
<script>
$('#register-form').submit(function(e){
		e.preventDefault();
        open_loader('#page');
               
		var form = $("#register-form")[0];
		var _data = new FormData(form);
		$.ajax({
			url: '{{url("/user/register/new")}}',
			data: _data,
			enctype: 'multipart/form-data',
			processData: false,
			contentType:false,
			type: 'POST',
			success: function(data){
				if(data.status == "success"){
					toastr.success(data.message, data.status);
					setTimeout("window.location.href='{{url('login')}}';",2000);
                    close_loader('#page');
                    } else{
                        toastr.error(data.message, data.status);
                        close_loader('#page');  
                    }
			},
			error: function(result){
				toastr.error('Check Your Network Connection !!!','Network Error');
                close_loader('#page');
			}
		});
		return false;
    });
 </script>
@endsection