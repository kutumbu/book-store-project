@extends( 'layouts.user' )

@section('title','Update Account')

@section('style')
@endsection

@section('content')
 <!-- Start Bradcaump area -->
 <div class="ht__bradcaump__area bg-image--6">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bradcaump__inner text-center">
                        	<h2 class="bradcaump-title">Update Account</h2>
                            <nav class="bradcaump-content">
                              <a class="breadcrumb_item" href="{{url('/')}}">Home</a>
                              <span class="brd-separetor">/</span>
                              <span class="breadcrumb_item active">Update Account</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <!-- Start Shop Page -->
        <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
					@include( 'user/include/menu' )
        			</div>
        			<div class="col-lg-9 col-12 order-1 order-lg-2">
					<div class="contact-form-wrap">
        					<h2 class="contact__title">Update Your Account</h2>
        					<p> </p>
                            <form id="edit-form">
                            {{ csrf_field() }}
                                <div class="single-contact-form space-between">
                                    <input type="text" name="fname" placeholder="First Name*" value="{{Auth::guard('user')->user()->fname}}">
                                    <input type="text" name="lname" placeholder="Last Name*" value="{{Auth::guard('user')->user()->lname}}">
                                </div>
                                <div class="single-contact-form space-between">
                                    <input type="email" name="email" placeholder="Email*" value="{{Auth::guard('user')->user()->email}}">
                                    <input type="text" name="tel" placeholder="Phone Number*" value="{{Auth::guard('user')->user()->tel}}">
                                </div>
                                <div class="single-contact-form space-between">
                                    <input type="text" name="country" placeholder="Country*" value="{{Auth::guard('user')->user()->country}}">
                                </div>
								<div class="single-contact-form space-between">
								    <input type="text" name="state" placeholder="State*" value="{{Auth::guard('user')->user()->state}}">
                                    <input type="text" name="city" placeholder="City*" value="{{Auth::guard('user')->user()->city}}">
                                </div>
								<div class="single-contact-form space-between">
                                    <input type="text" name="address" placeholder="Address*" value="{{Auth::guard('user')->user()->address}}">
                                </div>
								<div class="single-contact-form space-between">
								    <label>Profile Image</label>
                                    <input type="file" name="userpix" id="imgInp" accept="image/*">
									<img id="blah" src="{{asset('images/users/'.Auth::guard('user')->user()->userpix)}}" style="max-width: 100px; max-height: 100px"/>
                                </div>
                                 <div class="contact-btn">
								    <input type="hidden" name="id" value="{{Auth::guard('user')->user()->user_id}}">
                                    <button type="submit">Update Account</button>
                                </div>
                            </form>
                        </div> 
        			</div>
        		</div>
        	</div>
        </div>
        <!-- End Shop Page -->

@endsection

@section('script')
<script>
$('#edit-form').submit(function(e){
		e.preventDefault();
        open_loader('#page');
               
		var form = $("#edit-form")[0];
		var _data = new FormData(form);
		$.ajax({
			url: '{{url("/user/account/update")}}',
			data: _data,
			enctype: 'multipart/form-data',
			processData: false,
			contentType:false,
			type: 'POST',
			success: function(data){
				if(data.status == "success"){
					toastr.success(data.message, data.status);
					setTimeout("window.location.href='{{url('user/account/dashboard')}}';",2000);
                    close_loader('#page');
                    } else{
                        toastr.error(data.message, data.status);
                        close_loader('#page');  
                    }
			},
			error: function(result){
				toastr.error('Check Your Network Connection !!!','Network Error');
                close_loader('#page');
			}
		});
		return false;
    });

	function readURL(input) {

		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			$('#blah').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
		}
		}

		$("#imgInp").change(function() {
		readURL(this);
	});
 </script>
@endsection