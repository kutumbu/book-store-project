@extends( 'layouts.user' )

@section('title','Products')

@section('style')
@endsection

@section('content')
 <!-- Start Bradcaump area -->
 <div class="ht__bradcaump__area bg-image--6">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bradcaump__inner text-center">
                        	<h2 class="bradcaump-title">Account</h2>
                            <nav class="bradcaump-content">
                              <a class="breadcrumb_item" href="{{url('/')}}">Home</a>
                              <span class="brd-separetor">/</span>
                              <span class="breadcrumb_item active">Account</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <!-- Start Shop Page -->
        <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
					@include( 'user/include/menu' )
        			</div>
        			<div class="col-lg-9 col-12 order-1 order-lg-2">
					<button class="btn btn-info" style="float: right" data-toggle="modal" data-target="#product" type="button">Add New Product</button>
					<div class="container">
						<h2>Products</h2>
						
						<table class="table" id="datatable">
							<thead>
							<tr>
							    <th>#</th>
								<th>Product Name</th>
								<th>ISBN No</th>
								<th>Author</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>
							@foreach($product as $pr)
							<tr>
								<td><img src="{{asset('images/products/'.$pr->image)}}" style="max-width: 70px; max-height: 70px" alt=""></td>
								<td>{{$pr->pname}}</td>
								<td>{{$pr->isbn}}</td>
								<td>{{$pr->author}}n</td>
								@if($pr->status==ACTIVE)
								<td><span class="label label-success">Active</span></td>
								@else
								<td><span class="label label-warning">Inactive</span></td>
								@endif
								<td><div class="dropdown">
									<button class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
									<li><a href="javascript:void(0);" style="color: blue" data-target="#product-edit" data-toggle="modal" data-whatever="{{$pr->pid}}">Update</a></li>
									<li><a href="javascript:void(0);" style="color: red" onclick="deleteproduct('{{$pr->pid}}','{{$pr->pname}}')">Delete</a></li>
									</ul>
									</div>
							    </td>
							</tr>
							@endforeach
						    </tbody>
						</table>
						</div>
        			</div>
        		</div>
        	</div>
        </div>
        <!-- End Shop Page -->

<!-- =-=-=-=-=-=-= Edit Modal =-=-=-=-=-=-= -->
<div id="product-edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
			<div class="dash">

		</div>
	</div>
	</div>
</div>


		<!-- Modal -->
<div id="product" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title">Create New Product</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="product-form">
		{{ csrf_field() }}
           <div class="row">
                <div class="col-md-12">
				 <label>Product Name:</label>
                 <input class="form-control" type="text" name="pname" required>
				</div>
				<div class="col-md-12">
				 <label>ISBN No:</label>
                 <input class="form-control" type="text" name="isbn" required>
				</div>
				<div class="col-md-12">
				 <label>Category:</label>
                 <select class="form-control" name="cat_id" required>
				    <option value="1">Religious</option>
					<option value="2">Educational</option>
				 </select>
				</div>
				<div class="col-md-12">
				 <label>Author:</label>
                 <input class="form-control" type="text" name="author" required>
				</div>
				<div class="col-md-12">
				 <label>Product Price:</label>
                 <input class="form-control" type="text" name="price" required>
				</div>
				<div class="col-md-12">
				 <label>Short Description:</label>
                 <textarea class="form-control" name="short_descrpt" required></textarea>
				</div>
				<div class="col-md-12">
				 <label>Full Description:</label>
                 <textarea class="form-control" name="full_descrpt"></textarea>
				</div>
				<div class="col-md-12">
				 <label>Product Image:</label>
                 <input class="form-control" type="file" name="image" required>
				</div>&nbsp;&nbsp;

				<div class="col-md-12">
				<!-- <input class="form-control" type="hidden" name="user_id" value="{{Auth::guard('user')->user()->user_id}}" required> -->
				<button class="btn btn-info" type="submit">Submit</button>
				</div>
		   </div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection

@section('script')
<script>
      $('#product-form').submit(function(e){
		e.preventDefault();
        $('#product').modal('hide');
            open_loader('#page');
               
		var form = $("#product-form")[0];
		var _data = new FormData(form);
		$.ajax({
			url: '{{url("/user/product/new")}}',
			data: _data,
			enctype: 'multipart/form-data',
			processData: false,
			contentType:false,
			type: 'POST',
			success: function(data){
				//$("#blog").modal("toggle");
				if(data.status == "success"){
					toastr.success(data.message, data.status);
                    $( "#datatable" ).load( "{{url('user/account/products')}} #datatable" );
					//window.setTimeout(function(){location.reload();},2000);
                    close_loader('#page');
                    } else{
                        toastr.error(data.message, data.status);
                        close_loader('#page');  
                    }
			},
			error: function(result){
				toastr.error('Check Your Network Connection !!!','Network Error');
                close_loader('#page');
			}
		});
		return false;
    });

	function deleteproduct(id,name){
        open_loader('#page');  
                $.post('{{url("/user/product/delete")}}',
                {
                    _token:'{{csrf_token()}}',
                    id: id,
                    name: name
                },
                function(data){
                    if(data.status == "success"){
                    toastr.success(data.message, data.status);
                    $( "#datatable" ).load( "{{url('user/account/products')}} #datatable" );
                    close_loader('#page');
                   // window.setTimeout(function(){location.reload();},1000);
                } else{
                        toastr.error('Unsuccessful', 'Error deleting product');
                        close_loader('#page');  
                    }
                });
        }

    
      $('#product-edit').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var id = button.data('whatever') // Extract info from data-* attributes
          var modal = $(this);
          var dataString = 'id=' + id;
 
            $.ajax({
                type: "GET",
                url: "/user/product/data/",
                data: dataString,
                cache: false,
                success: function (data) {
                    //console.log(data);
                    modal.find('.dash').html(data);
                },
                error: function(err) {
                    //console.log(err);
                }
            });  
    })


    function checkAllContestant(){
    var ch =document.getElementById('chAllCon').checked,
    checked = false;
    if(ch){
        checked=true;
    }
        var els = document.getElementsByClassName('contestantBox');
        
        for(var g=0;g<els.length;g++){
            els[g].checked=checked;
        }
        
        
    }

    $(function () {
        // when the modal is closed
        $('#product-edit').on('hidden.bs.modal', function () {
            // remove the bs.modal data attribute from it
            $(this).removeData('bs.modal');
            // and empty the modal-content element
            $('#modal-container .modal-content').empty();
        }); 
    });

    
</script>
@endsection