



<div class="modal-header">
	  <h4 class="modal-title">Update Product</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="edit-form">
		{{ csrf_field() }}
           <div class="row">
                <div class="col-md-12">
				 <label>Product Name:</label>
                 <input class="form-control" type="text" name="pname" value="{{$product->pname}}" required>
				</div>
				<div class="col-md-12">
				 <label>ISBN No:</label>
                 <input class="form-control" type="text" name="isbn" value="{{$product->isbn}}" required>
				</div>
				<div class="col-md-12">
				 <label>Category:</label>
                 <select class="form-control" name="cat_id" required>
                    <option value="{{$product->cat_id}}">{{$product->cat_id}}</option>
				    <option value="1">Religious</option>
					<option value="2">Educational</option>
				 </select>
				</div>
				<div class="col-md-12">
				 <label>Author:</label>
                 <input class="form-control" type="text" name="author" value="{{$product->author}}" required>
				</div>
				<div class="col-md-12">
				 <label>Product Price:</label>
                 <input class="form-control" type="text" name="price" value="{{$product->price}}" required>
				</div>
				<div class="col-md-12">
				 <label>Short Description:</label>
                 <textarea class="form-control" name="short_descrpt" required>{{$product->short_descrpt}}</textarea>
				</div>
				<div class="col-md-12">
				 <label>Full Description:</label>
                 <textarea class="form-control" name="full_descrpt">{{$product->full_descrpt}}</textarea>
				</div>
				<div class="row">
				 <div class="col-md-9">
                 <label>Product Image:</label>
                    <input type="file" name="image" id="imgInp2" class="form-control">
                </div>
                <div class="col-md-3">
                    <img id="blah2" src="{{asset('images/products/'.$product->image)}}" style="max-width: 100px; max-height: 100px"/>
                </div>
				</div>&nbsp;&nbsp;

				<div class="col-md-12">
				<input class="form-control" type="hidden" name="id" id="id" value="{{$product->pid}}" required="">
				<button class="btn btn-info" type="submit">Submit</button>
				</div>
		   </div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>


      <script>
function readURL(input) {

if (input.files && input.files[0]) {
var reader = new FileReader();

reader.onload = function(e) {
    $('#blah2').attr('src', e.target.result);
}

reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp2").change(function() {
readURL(this);
});


$('#edit-form').submit(function(e){
		e.preventDefault();
        $('#product-edit').modal('hide');
            open_loader('#page');
               
		var form = $("#edit-form")[0];
		var _data = new FormData(form);
		$.ajax({
			url: '{{url("/user/product/update")}}',
			data: _data,
			enctype: 'multipart/form-data',
			processData: false,
			contentType:false,
			type: 'POST',
			success: function(data){
				//$("#blog").modal("toggle");
				if(data.status == "success"){
					toastr.success(data.message, data.status);
                    $( "#datatable" ).load( "{{url('/user/account/products')}} #datatable" );
					//window.setTimeout(function(){location.reload();},2000);
                    close_loader('#page');
                    } else{
                        toastr.error(data.message, data.status);
                        close_loader('#page');  
                    }
			},
			error: function(result){
				toastr.error('Check Your Network Connection !!!','Network Error');
                close_loader('#page');
			}
		});
		return false;
    });

</script>