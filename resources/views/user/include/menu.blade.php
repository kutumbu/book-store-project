<div class="shop__sidebar">
	<aside class="wedget__categories poroduct--cat">
	@if(!empty(Auth::guard('user')->user()->user_id))
	    <img src="{{asset('images/users/'.Auth::guard('user')->user()->userpix)}}" style="max-width: 150px; max-height: 150px; border-radius: 50%"/><br><br>
	@endif
		<ul>
			<li><a href="{{url('user/account/dashboard')}}">Account </a></li>
			<li><a href="{{url('user/account/update')}}">Update Account</a></li>
			<li><a href="{{url('user/account/products')}}">Products</a></li>
			<li><a href="{{url('user/account/change-password')}}">Change Password</a></li>
			<li><a href="{{url('user/account/articles')}}">Articles </a></li>
			<li><a href="{{url('user/account/wishlist')}}">Wishlist </a></li>
			<li><a href="{{url('user/logout')}}">Logout </a></li>
		</ul>
	</aside>
	
</div>