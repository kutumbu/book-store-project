@extends( 'layouts.user' )

@section('title','Change Password')

@section('style')
@endsection

@section('content')
 <!-- Start Bradcaump area -->
 <div class="ht__bradcaump__area bg-image--6">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bradcaump__inner text-center">
                        	<h2 class="bradcaump-title">Change Password</h2>
                            <nav class="bradcaump-content">
                              <a class="breadcrumb_item" href="{{url('/')}}">Home</a>
                              <span class="brd-separetor">/</span>
                              <span class="breadcrumb_item active">Change Password</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <!-- Start Shop Page -->
        <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
					@include( 'user/include/menu' )
        			</div>
        			<div class="col-lg-9 col-12 order-1 order-lg-2">
                    <div class="contact-form-wrap">
        					<h2 class="contact__title">Change Account Password</h2>
        					<p></p>
                            <form id="pass-form">
                            {{ csrf_field() }}
                            <div class="single-contact-form space-between">
                                   <input type="password" name="curpass" placeholder="Current Password*" required="required">
                                </div>
                                <div class="single-contact-form space-between">
                                    <input type="password" name="newpass" id="pass1" placeholder="New Password*" required="required">
                                    <input type="password" name="newpass2" id="pass2" onkeyup="checkPass(); return false;" placeholder="Confirm Password*" required="required">
                                    <span id="confirmMessage" class="confirmMessage"></span>
                                </div>
                               <div class="contact-btn">
                                    <button type="submit">Change Password</button>
                                </div>
                            </form>
                        </div> 
        			</div>
        		</div>
        	</div>
        </div>
        <!-- End Shop Page -->

@endsection

@section('script')
<script>

$('#pass-form').submit(function(e){
		e.preventDefault();
        //$('#admin-edit').modal('hide');
            open_loader('#page');
               
		var form = $("#pass-form")[0];
		var _data = new FormData(form);
		$.ajax({
			url: '{{url("/user/password/update")}}',
			data: _data,
			enctype: 'multipart/form-data',
			processData: false,
			contentType:false,
			type: 'POST',
			success: function(data){
				//$("#blog").modal("toggle");
				if(data.status == "success"){
					toastr.success(data.message, data.status);
					window.setTimeout(function(){location.reload();},2000);
                    close_loader('#page');
                    } else{
                        toastr.error(data.message, data.status);
                        close_loader('#page');  
                    }
			},
			error: function(result){
				toastr.error('Check Your Network Connection !!!','Network Error');
                close_loader('#page');
			}
		});
		return false;
    });


    function checkPass(){
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}  

</script>
@endsection