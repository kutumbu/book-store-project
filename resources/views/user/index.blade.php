@extends( 'layouts.user' )

@section('title','Products')

@section('style')
@endsection

@section('content')
 <!-- Start Bradcaump area -->
 <div class="ht__bradcaump__area bg-image--6">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bradcaump__inner text-center">
                        	<h2 class="bradcaump-title">Account</h2>
                            <nav class="bradcaump-content">
                              <a class="breadcrumb_item" href="{{url('/')}}">Home</a>
                              <span class="brd-separetor">/</span>
                              <span class="breadcrumb_item active">Account</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <!-- Start Shop Page -->
        <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
					@include( 'user/include/menu' )
        			</div>
        			<div class="col-lg-9 col-12 order-1 order-lg-2">
                    <div class="content">
        					<h3>{{Auth::guard('user')->user()->fname}} {{Auth::guard('user')->user()->lname}}</h3><br>
        					<strong>Email Address</strong>
        					<p>{{Auth::guard('user')->user()->email}}</p><br>
                            <strong>Phone Number</strong>
        					<p>{{Auth::guard('user')->user()->tel}}</p><br>
                            <strong>Country</strong>
        					<p>{{Auth::guard('user')->user()->country}}</p><br>
                            <strong>State</strong>
        					<p>{{Auth::guard('user')->user()->state}}</p><br>
                            <strong>City</strong>
        					<p>{{Auth::guard('user')->user()->city}}</p><br>
                            <strong>Address</strong>
        					<p>{{Auth::guard('user')->user()->address}}</p><br>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <!-- End Shop Page -->

@endsection

@section('script')
@endsection