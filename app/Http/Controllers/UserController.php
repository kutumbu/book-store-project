<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use App\Admin;
use App\Company;
use App\AutoReply;
use App\Advert;
use App\User;
use App\Product;
use App\blog;
use App\blogcat;
use App\blogcomment;
use App\cart;
use App\contact;
use App\newsletter;
use App\order;
use App\orderedProduct;
use App\pcat;
use App\productcomment;
use App\slide;
use App\wishlist;
use App\messageSetting;

class UserController extends Controller
{
        
    public function createproduct(Request $request){
		$pname = $request->pname;
		$isbn = $request->isbn;
		$short_descrpt = $request->short_descrpt;
        $full_descrpt = $request->full_descrpt;
        $author = $request->author;
        $cat_id = $request->cat_id;
        $price = $request->price;
        $user_key = Auth::guard("user")->user()->user_id;
		
		$image = $request->file('image');
		$imageName  = time() . '.' . $image->getClientOriginalExtension();
		$path = "images/products";
        $image->move($path, $imageName);
        
		$item = new Product();
        $item->pname = $pname;
        $item->user_id = $user_key;
        $item->cat_id = $cat_id;
        $item->isbn = $isbn;
        $item->author = $author;
        $item->price = $price;
        $item->sku = str_random(6);
        $item->url = preg_replace("/[^\da-z]/i","-",  substr(strtolower($pname),0,200))."-".str_random(5);
        $item->status = ACTIVE;
        $item->featured = NO;
        $item->short_descrpt = $short_descrpt;
		$item->full_descrpt = $full_descrpt;
		$item->image = $imageName;
		
		if($item->save()){

		$response = array(
			"status" => "success",
			"message" => "Productt was created successfully",
		);
		
		//$this->log("Added new Project. Title - ".$pname);
        return Response::json($response); //return status response as json
    } else {
        $response = array(
			"status" => "unsuccessful",
			"message" => "Error creating product. Please try again",
		);
		return Response::json($response); //return status response as json
    }
    }

    public function updateproduct(Request $request){
		$id = $request->id;
        $pname = $request->pname;
		$isbn = $request->isbn;
		$short_descrpt = $request->short_descrpt;
        $full_descrpt = $request->full_descrpt;
        $author = $request->author;
        $cat_id = $request->cat_id;
        $price = $request->price;
		
		$image = $request->file('image');
                if(!is_null($image) && $image != ''){
                    $imageName  = time() . '.' . $image->getClientOriginalExtension();
                    $path = "images/products";
                    $image->move($path, $imageName);
                }
		
		$item = Product::where('pid',$id)->first();
		$item->pname = $pname;
        $item->short_descrpt = $short_descrpt;
		$item->full_descrpt = $full_descrpt;
        $item->cat_id = $cat_id;
        $item->isbn = $isbn;
        $item->author = $author;
        $item->price = $price;
               if(!is_null($image) && $image != ''){
                    $item->image = $imageName;
                }
		if($item->save()){
		
		$response = array(
			"status" => "success",
			"message" => "product updated successfully",
        );
       // $this->log("Partner details updated. Name - ".$name);
        return Response::json($response); //return status response as json
    } else {
        $response = array(
			"status" => "unsuccessful",
			"message" => "Error updating product",
        );
        return Response::json($response); //return status response as json
    }
		
    }

    public function getproducts(Request $request){
        $product = Product::select('products.*')
            ->where('user_id',Auth::guard('user')->user()->user_id)
            ->get();
    return view('user/product', compact('product'));
		
    }

    public function getproductdata(Request $request){
        $id = $request->id;
        $product = Product::where('pid',$id)->first();
        
        return view('user/modals/product-edit', compact('product'));
		
    }

    public function deleteproduct(Request $request) {
        $id = $request->id;
        //$name = $request->name;
        
        
        $item = Product::where('pid',$id)->delete();
        
        $response = array(
            "status" => "success",
            "message" => "Product deleted",
        );
        
        //$this->log("Blog Post deleted - ".$name);
        
        return Response::json($response); //return status response as json
        }

        public function productsearch(Request $request){
            $q = $request->input('q');
            $product = Product::where ( 'pname', 'LIKE', '%' . $q . '%' )->orWhere ( 'isbn', '=', $q)->get ();
            //$product = Product::paginate(4);
            //return view ("blog-search")->with('blog',$blog)->with('q',$q);
            return view('search', compact('product','q'));
        }



        public function updateaccount(Request $request){
            $id = $request->id;
            $fname = $request->fname;
            $lname = $request->lname;
            $tel = $request->tel;
            $email = $request->email;
            $country = $request->country;
            $state = $request->state;
            $city = $request->city;
            $address = $request->address;

            $image = $request->file('userpix');
                if(!is_null($image) && $image != ''){
                    $imageName  = time() . '.' . $image->getClientOriginalExtension();
                    $path = "images/users";
                    $image->move($path, $imageName);
                }
            
            $admin = User::where('user_id',$id)->first();
            $admin->fname = $fname;
            $admin->lname = $lname;
            $admin->tel = $tel;
            $admin->email = $email;
            $admin->country = $country;
            $admin->state = $state;
            $admin->city = $city;
            $admin->address = $address;
            if(!is_null($image) && $image != ''){
                $admin->userpix = $imageName;
            }
            
                if($admin->save()){
                
                $response = array(
                    "status" => "success",
                    "message" => "Account details updated successfully.",
                );
                
                return Response::json($response); //return status response as json
            } else {
                $response = array(
                    "status" => "Unsuccessfull",
                    "message" => "Error updating account. please try again",
                );
                return Response::json($response); //return status response as json
            }
        }

        public function changepassword(Request $request){
            $user = Auth::guard('user')->user();
            $old = $request->curpass;
            $newp = $request->newpass;
                
            if($newp == "" || $old == ""){
                $response = array(
                    'status' => 'error',
                    'message' => 'Empty password field entered',
                );
                return Response::json($response); 
            }
            elseif(Hash::check($old, $user->password)){
                $user->password = bcrypt($newp);
                $user->save();
                $response = array(
                    'status' => 'success',
                    'message' => 'Password changed successfully',
                );
                //$this->log("Changed their password. ID - ".$user->user_id);
                return Response::json($response); 
            }
            else{
                $response = array(
                    'status' => 'error',
                    'message' => 'Invalid password entered',
                );
                return Response::json($response); 
            }
        }

}
