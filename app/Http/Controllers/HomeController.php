<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use App\Admin;
use App\Company;
use App\AutoReply;
use App\Advert;
use App\User;
use App\Product;
use App\blog;
use App\blogcat;
use App\blogcomment;
use App\cart;
use App\contact;
use App\newsletter;
use App\order;
use App\orderedProduct;
use App\pcat;
use App\productcomment;
use App\slide;
use App\wishlist;
use App\messageSetting;

class HomeController extends Controller
{
    //
}
