<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('create-account', function () {
    return view('register');
});
Route::get('login', function () {
    return view('login');
});
Route::get('about-us', function () {
    return view('about');
});
Route::get('shopping-cart', function () {
    return view('cart');
});
Route::get('checkout', function () {
    return view('checkout');
});
Route::get('contact-us', function () {
    return view('contact');
});
Route::get('products/all', function () {
    return view('products');
});
Route::get('product/', function () {
    return view('product-details');
});
Route::get('search', function () {
    return view('search');
});
Route::get('blog/all/', function () {
    return view('blog');
});
Route::get('blog/', function () {
    return view('blog-details');
});
Route::get('user/account/dashboard', function () {
    return view('user/index');
});
Route::get('user/account/products', function () {
    return view('user/product');
});
Route::get('user/account/update', function () {
    return view('user/update');
});
Route::get('user/account/articles', function () {
    return view('user/article');
});
Route::get('user/account/change-password', function () {
    return view('user/change-password');
});
Route::get('search', function () {
    return view('search');
});
Route::get('user/account/wishlist', function () {
    return view('user/wishlist');
});





//Login and register routes
Route::post('/admin/portal/login', 'AuthController@adminlogin');
Route::get('/admin/logout', 'AuthController@adminlogout');
Route::post('/user/register/new', 'AuthController@registeruser');
Route::post('/user/login/new', 'AuthController@userlogin');
Route::get('/user/logout', 'AuthController@userlogout');


//user routes
Route::get('/user/account/products', 'UserController@getproducts');
Route::get('/user/product/data/', 'UserController@getproductdata');
Route::post('/user/product/new', 'UserController@createproduct');
Route::post('/user/product/update', 'UserController@updateproduct');
Route::post('/user/product/delete', 'UserController@deleteproduct');
Route::post('/home/product/search', 'UserController@productsearch');
Route::post('/user/account/update', 'UserController@updateaccount');
Route::post('/user/password/update', 'UserController@changepassword');